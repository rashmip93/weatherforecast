import "./App.css";
import React, { useState } from "react";
import Header from "./components/Header";
import List from "./components/List";

function App() {
  const [cityName, setCityName] = useState("Pune");

  return (
    <div>
      <Header cityName={cityName} setCityName={setCityName} />
      <List searchValue={cityName} />
    </div>
  );
}

export default App;
