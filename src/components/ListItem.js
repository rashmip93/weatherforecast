import React from "react";
import { Accordion, Card } from "react-bootstrap";
import CardHeader from "./CardHeader";
import CardContent from "./CardContent";

const ListItem = ({ dataList }) => {
  function AccordianList() {
    const listItems = dataList.map((data, index) => (
      // Correct! Key should be specified inside the array.
      <Card key={index.toString()}>
        <Accordion.Toggle as={Card.Header} eventKey={index.toString()}>
          <CardHeader data={data} />
        </Accordion.Toggle>
        <Accordion.Collapse eventKey={index.toString()}>
          <Card.Body style={{ padding: 0 }}>
            <CardContent data={data} />
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    ));
    return <ul>{listItems}</ul>;
  }
  return (
    <Accordion>
      <AccordianList />
    </Accordion>
  );
};

export default ListItem;
