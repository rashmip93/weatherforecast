import React, { Component } from "react";
import ListItem from "./ListItem";
import axios from "axios";
import moment from "moment";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      weatherData: null,
      list: [],
    };
  }

  sortData = (list) => {
    let final = list.map((item) => {
      return Object.assign(item, {
        date: moment(item.dt_txt).format("ddd DD"),
      });
    });

    // // Group by color as key to the person array
    const personGroupedByColor = this.groupBy(final, "date");
    // console.log("personGroupedByColor: ", personGroupedByColor);

    let arr = Object.keys(personGroupedByColor).map((k) => {
      return {
        date: k,
        list: personGroupedByColor[k],
      };
    });
    // console.log("arr: ", arr);
    this.setState({ list: arr });
  };

  // // Accepts the array and key
  groupBy = (array, key) => {
    // Return the end result
    return array.reduce((result, currentValue) => {
      // If an array already present for key, push it to the array. Else create an array and push the object
      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue
      );
      // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
      return result;
    }, {}); // empty object is the initial value for result object
  };

  componentDidMount = async () => {
    try {
      const { searchValue } = this.props;
      this.setState({ loading: true });
      let response = await this.getWeatherForecastData(searchValue);
      this.sortData(response.data.list);
      this.setState({ loading: false, weatherData: response.data });
    } catch (error) {
      console.log("error: ", error);
    }
  };

  componentDidUpdate = async (prevProp, prevState) => {
    const { searchValue } = this.props;
    if (prevProp.searchValue !== searchValue) {
      try {
        let response = await this.getWeatherForecastData(searchValue);
        this.sortData(response.data.list);
        this.setState({ loading: false, weatherData: response.data });
      } catch (error) {
        console.log("Invalid Input");
      }
    }
  };

  getWeatherForecastData = (searchValue) => {
    return new Promise((resolve, reject) => {
      try {
        axios
          .get(
            `https://api.openweathermap.org/data/2.5/forecast?q=${searchValue}&units=metric&appid=e5d9b568ee194940f9c52be6167f17d2`
          )
          .then((res) => {
            resolve(res);
          })
          .catch((e) => {
            reject(e);
            console.log(e);
          });
      } catch (error) {
        reject(error);
      }
    });
  };
  render() {
    // console.log("searchValue", this.props.searchValue);
    const { loading } = this.state;

    if (loading) {
      return (
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-info" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      );
    }
    return (
      <div class="card" style={{ margin: 20 }}>
        <div class="card-body">
          <h5 class="card-title">
            5-Day / 3 hour Weather - {this.state.weatherData?.city?.name}
          </h5>
        </div>
        <ListItem dataList={this.state.list} />
      </div>
    );
  }
}

export default List;
