import React from "react";
import moment from "moment";

const CardContent = ({ data: { list } }) => {
  function Detail() {
    const listItems = list.map((item, index) => (
      <div key={index.toString()} class="col" style={{ padding: "10px" }}>
        <div style={{ padding: "10px" }}>
          <div>
            <span style={{ fontSize: 12 }}>
              {moment(item.dt_txt).format("ddd DD")}
            </span>{" "}
            |
            <span style={{ fontWeight: "500" }}>
              {moment(item.dt_txt).format("HH:mm")}
            </span>
          </div>
          <div>
            <span style={{ fontWeight: "bold", fontSize: "1.5rem" }}>
              {parseInt(item.main.temp)} &#8451;
            </span>
          </div>
          <div style={{ fontWeight: "500" }}>
            It feels like {item.main.feels_like} &#8451;.
            <span style={{ paddingLeft: 2, textTransform: "capitalize" }}>
              {item.weather[0].description}
            </span>
          </div>
        </div>
        <div
          style={{
            borderWidth: "thin",
            borderStyle: "inset",
            borderColor: "grey",
            borderRadius: "10px",
            padding: "10px",
          }}
        >
          <div class="row">
            <div class="col">
              <span style={{ fontSize: 12 }}>Humidity</span>
            </div>
            <div class="col">
              <span style={{ fontSize: 12 }}>Visibility</span>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <span style={{ fontSize: 12, fontWeight: "bold" }}>
                {item.main.humidity}
              </span>
            </div>
            <div class="col">
              <span style={{ fontSize: 12, fontWeight: "bold" }}>
                {item.visibility}
              </span>
            </div>
          </div>
        </div>
      </div>
    ));
    return <>{listItems}</>;
  }

  return (
    <div class="container-fluid">
      <div class="row">
        <Detail />
      </div>
    </div>
  );
};

export default CardContent;
