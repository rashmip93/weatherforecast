import React from "react";

const CardHeader = ({ data: { date } }) => {
  return (
    <div>
      <span style={{ color: "#117a8b", fontWeight: "bold" }}>{date}</span>
    </div>
  );
};

export default CardHeader;
