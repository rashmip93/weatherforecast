import React, { useState } from "react";
import { Button } from "react-bootstrap";

const Header = ({ cityName, setCityName }) => {
  const [name, setName] = useState("");

  // const handleKeyDown = (event) => {
  //   if (event.key === "Enter") {
  //     console.log("do validate");
  //     name && setCityName(name);
  //   }
  //   // event.preventDefault();
  // };

  return (
    <div
      style={{
        backgroundColor: "#117a8b",
        height: "50px",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        color: "white",
        padding: 10,
      }}
    >
      <span style={{ fontWeight: "bold" }}>Weather forecast</span>

      <form class="form-inline">
        <input
          class="form-control form-control-sm"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Search by City"
          aria-label="Search"
          // onKeyDown={handleKeyDown}
        />
        <Button variant="info" size="sm" onClick={() => setCityName(name)}>
          search
        </Button>
      </form>
    </div>
  );
};

export default Header;
